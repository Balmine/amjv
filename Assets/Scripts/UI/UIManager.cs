using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Audio;
using TMPro;

public class UIManager : MonoBehaviour
{
    public GameObject startCanvas;
    public GameObject optionsCanvas;
    public GameObject rulesCanvas;

    [SerializeField] public Slider effectsVolumeSlider;
    public Slider musicVolumeSlider;
    public Slider masterVolumeSlider;
    public GameObject graphicsQualityDropdownGO;
    public GameObject resolutionDropdownGO;
    public TMP_Dropdown graphicsQualityDropdown;
    public TMP_Dropdown resolutionDropdown;

    public AudioMixer audioMixer;
    float effectsVolume, musicVolume, masterVolume;



    void Start()
    {

        // Load the AudioMixer
        float effectsVolume = PlayerPrefs.GetFloat("EffectsVolume", 0.5f);
        float musicVolume = PlayerPrefs.GetFloat("MusicVolume", 0.5f);
        float masterVolume = PlayerPrefs.GetFloat("Master", 0.5f);

        // Set the slider values
        effectsVolumeSlider.value = effectsVolume;
        musicVolumeSlider.value = musicVolume;
        masterVolumeSlider.value = masterVolume;

        // Set the values in the AudioMixer
        audioMixer.SetFloat("EffectsVolume", Mathf.Log10(effectsVolume) * 20);
        audioMixer.SetFloat("MusicVolume", Mathf.Log10(musicVolume) * 20);
        audioMixer.SetFloat("Master", Mathf.Log10(masterVolume) * 20);
        graphicsQualityDropdown = graphicsQualityDropdownGO.GetComponent<TMP_Dropdown>();
        resolutionDropdown = resolutionDropdownGO.GetComponent<TMP_Dropdown>();

        graphicsQualityDropdown.value = PlayerPrefs.GetInt("GraphicsQuality", 2);
        resolutionDropdown.value = PlayerPrefs.GetInt("Resolution", 0);
        ShowStartCanvas();
    }

    public void ShowStartCanvas()
    {
        startCanvas.SetActive(true);
        optionsCanvas.SetActive(false);
        rulesCanvas.SetActive(false);
    }

    public void ShowOptionsCanvas()
    {
        startCanvas.SetActive(false);
        optionsCanvas.SetActive(true);
        rulesCanvas.SetActive(false);
    }

    public void ShowRulesCanvas()
    {
        startCanvas.SetActive(false);
        rulesCanvas.SetActive(true);
        optionsCanvas.SetActive(false);
    }

    public void StartGame()
    {
        SceneManager.LoadScene("Map 1");
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void UpdateEffectsVolume()
    {
        float dB = LinearToDecibel(effectsVolumeSlider.value);
        audioMixer.SetFloat("EffectsVolume", dB);
        PlayerPrefs.SetFloat("EffectsVolume", effectsVolumeSlider.value);
    }

    public void UpdateMusicVolume()
    {
        float dB = LinearToDecibel(musicVolumeSlider.value);
        audioMixer.SetFloat("MusicVolume", dB);
        PlayerPrefs.SetFloat("MusicVolume", musicVolumeSlider.value);
    }

    public void UpdateMasterVolume()
    {
        float dB = LinearToDecibel(masterVolumeSlider.value);
        audioMixer.SetFloat("Master", dB);
        PlayerPrefs.SetFloat("Master", masterVolumeSlider.value);
    }

    public void UpdateGraphicsQuality()
    {
        string selectedOption = graphicsQualityDropdown.options[graphicsQualityDropdown.value].text;
        int qualityIndex;

        switch (selectedOption)
        {
            case "Low":
                qualityIndex = 1;
                break;
            case "Medium":
                qualityIndex = 2;
                break;
            case "High":
                qualityIndex = 3;
                break;
            default:
                qualityIndex = 3;
                break;
        }

        QualitySettings.SetQualityLevel(qualityIndex);
        PlayerPrefs.SetInt("GraphicsQuality", qualityIndex);
    }

    public void UpdateResolution()
    {
        int width, height;

        switch (resolutionDropdown.value)
        {
            case 0:
                width = 1280;
                height = 720;
                break;
            case 1:
                width = 1920;
                height = 1080;
                break;
            case 2:
                width = 3840;
                height = 2160;
                break;
            default:
                width = 1920;
                height = 1080;
                break;
        }

        Screen.SetResolution(width, height, FullScreenMode.Windowed);
        PlayerPrefs.SetInt("Resolution", resolutionDropdown.value);
    }
    public static float LinearToDecibel(float linear)
    {
        float dB;

        if (linear != 0)
            dB = 20.0f * Mathf.Log10(linear) + 10f;
        else
            dB = -80.0f;

        return Mathf.Clamp(dB, -80.0f, 20.0f);
    }

    public void UpdateDifficulty()
    {

        // A faire une fois que les ennemis sont implémentés 
    }

}