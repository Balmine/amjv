using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using System.Text;
using UnityEngine.Audio;





public class UIManagerGame : MonoBehaviour
{
    public TMP_Text gameTimeText;
    public TMP_Text enemiesRemainingText;

    public RawImage CadreEnnemies;
    public RawImage CadreTime;

    private float gameTime;
    private bool gameIsOver = false;

    public GameObject pauseMenu;
    public Slider volumeSlider;
    private bool isPaused = false;

    public UnitController unitController;

    public TMP_Text unitsText;

    public AudioClip musicClip;
    public float musicVolume = 1.0f;
    private AudioSource audioSource;
    public AudioMixer audioMixer;
    public Slider musicVolumeSlider;


    void Start()
    {
        ResumeGame();
        StartCoroutine(UpdateEnemiesRemainingEverySecond());
        enemiesRemainingText.enabled = false;
        CadreEnnemies.enabled = false;
        CadreTime.enabled = false;
        gameTimeText.enabled = false;
        audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.clip = musicClip;
        audioSource.loop = true;
        audioSource.outputAudioMixerGroup = audioMixer.FindMatchingGroups("MusicVolume")[0];
        float savedVolume = PlayerPrefs.GetFloat("MusicVolume");
        float dB = LinearToDecibel(savedVolume);
        musicVolumeSlider.value = savedVolume;

        audioMixer.SetFloat("MusicVolume", dB);

        audioSource.Play();
    }

    void Update()
    {

        UpdateUnitsToShow();
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            isPaused = !isPaused;
            pauseMenu.SetActive(isPaused);
            Time.timeScale = isPaused ? 0 : 1;

            if (isPaused)
            {
                audioSource.Pause();
            }
            else
            {
                audioSource.UnPause();
            }
        }
        gameTime += Time.deltaTime;
        TimeSpan timeSpan = TimeSpan.FromSeconds(gameTime);
        gameTimeText.text = string.Format("{0:D2}:{1:D2}", timeSpan.Minutes, timeSpan.Seconds);


    }


    public void UpdateEnemiesRemaining()
    {
        int enemiesRemaining = 0;
        GameObject[] enemyObjects = GameObject.FindGameObjectsWithTag("enemy");
        enemiesRemaining = enemyObjects.Length;
        enemiesRemainingText.text = enemiesRemaining.ToString();
    }

    IEnumerator UpdateEnemiesRemainingEverySecond()
    {
        yield return new WaitForSeconds(0.1f); //Attendre que les unités soient enregistrées
        enemiesRemainingText.enabled = true;
        CadreEnnemies.enabled = true;
        CadreTime.enabled = true;
        gameTimeText.enabled = true;

        while (!gameIsOver)
        {
            UpdateEnemiesRemaining();
            yield return new WaitForSeconds(1);
        }
    }

    public void QuitGame()
    {
        SceneManager.LoadScene("StartMenu");
    }

    public void ResumeGame()
    {
        isPaused = false;
        pauseMenu.SetActive(false);
        Time.timeScale = 1;
    }
    public void UpdateUnitsToShow()
    {
        StringBuilder sb = new StringBuilder();
        sb.AppendLine("<b><u>Selected Units:</u></b> \n");
        int count = 0;

        foreach (Unit unit in unitController.unitsToMove)
        {
            if (count < 5)
            {
                sb.AppendLine(unit.name);
            }
            count++;
        }
        if (count > 5)
        {
            sb.AppendLine($"+ {count - 5} more");
        }
        unitsText.text = sb.ToString();
    }

    public void UpdateMusicVolume()
    {
        float dB = LinearToDecibel(musicVolumeSlider.value);
        audioMixer.SetFloat("MusicVolume", dB);
        PlayerPrefs.SetFloat("MusicVolume", musicVolumeSlider.value);
    }

    public static float LinearToDecibel(float linear)
    {
        float dB;

        if (linear != 0)
            dB = 20.0f * Mathf.Log10(linear) + 10f;
        else
            dB = -80.0f;

        return Mathf.Clamp(dB, -80.0f, 20.0f);
    }

}
