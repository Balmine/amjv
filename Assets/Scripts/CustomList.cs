using UnityEngine;
using System.Collections.Generic;

public class CustomList<Unit> : List<Unit>
{
    public new void Add(Unit item)
    {
        if (!Contains(item))
        {
            base.Add(item);
        }
        else
        {
            Debug.LogWarning("Element already exists in the list.");
        }
    }

    public new bool Remove(Unit item)
    {
        if (Contains(item))
        {
            return base.Remove(item);
        }
        else
        {
            Debug.LogWarning("Element not found in the list.");
            return false;
        }
    }

    public new bool Contains(Unit item)
    {
        return base.Contains(item);
    }



}