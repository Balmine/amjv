using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public Unit unit;

    public Enemy enemy;
    public Canvas canvas;
    public Image healthBar;

    private float healthBarWidth;

    private Camera mainCamera;

    public GameObject redBar;

    private Vector3 unitPosition;
    public float updateSpeed;

    private Image redBarImage;

    private void Start()
    {
        redBarImage = redBar.GetComponent<Image>();

        mainCamera = Camera.main;
        updateSpeed = 5.0f;

    }

    private void Update()
    {
        if (unit == null && enemy == null)
        {
            Destroy(redBar);
            Destroy(gameObject);
            return;
        }
        if (enemy == null)
        {
            unitPosition = mainCamera.WorldToScreenPoint(unit.transform.position + Vector3.up * 2.0f);
            float targetFillAmount = (float)unit.health / unit.maxHealth;
            transform.position = unitPosition;
            redBar.transform.position = unitPosition;
            healthBar.fillAmount = Mathf.Lerp(healthBar.fillAmount, targetFillAmount, updateSpeed * Time.deltaTime);
            healthBar.enabled = (float)targetFillAmount != 1.0f;
            redBarImage.enabled = (float)targetFillAmount != 1.0f;
        }
        else if (unit == null)
        {

            unitPosition = mainCamera.WorldToScreenPoint(enemy.transform.position + Vector3.up * 2.0f);
            float targetFillAmount = (float)enemy.CurrentHealth / enemy.MaxHealth;
            transform.position = unitPosition;
            redBar.transform.position = unitPosition;
            healthBar.fillAmount = Mathf.Lerp(healthBar.fillAmount, targetFillAmount, updateSpeed * Time.deltaTime);
            healthBar.enabled = (float)targetFillAmount != 1.0f;
            redBarImage.enabled = (float)targetFillAmount != 1.0f;

        }




    }

}