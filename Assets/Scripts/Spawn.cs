using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{
    public RTSManager RTSManager;

    private void OnTriggerEnter(Collider other)
    {
        // V�rifier si l'objet en collision est une instance de la classe "Unit"
        if (other.gameObject.tag == "unit")
        {
            Unit unit = other.GetComponent<Unit>();

            // V�rifier si l'unit� a le drapeau
            if (unit.hasFlag)
            {
                // L'unit� avec le drapeau est dans la zone
                Debug.Log("Unit with flag entered the capture zone!");
                RTSManager.TriggerVictory();

                // Ajoutez ici le code que vous souhaitez ex�cuter lorsque l'unit� avec le drapeau entre dans la zone.
            }
        }
    }
}
