using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UIElements;

public class Flag : MonoBehaviour
{
    public bool isCaptured;
    public string side;
    public bool inBase;
    public Vector3 flagSpawn;
    // M�thode appel�e lorsqu'une collision se produit
    private void OnTriggerEnter(Collider other)
    {
        // V�rifier si l'objet en collision est une instance de la classe "Unit"
        if (other.gameObject.tag == "unit" && !isCaptured)
        {
            Unit unit = other.gameObject.GetComponent<Unit>();
            // si unit� alli� et pas dans la base, le tp au spawn
            if (this.side == unit.side)
            {
                float distanceToTarget = Vector3.Distance(transform.position, flagSpawn);

                if (distanceToTarget >= 10f)
                {
                    this.transform.localPosition = flagSpawn;
                    Debug.Log("Flag returned to spawn");
                }
            }
            // si unit� ennemie, le ddrapeau est captur�
            else
            {
                SetRenduObjet(false);
                isCaptured = true;
                unit.hasFlag = true;
                unit.ChangeMaterialWhenFlag();
                this.transform.parent = unit.transform;
            }
        }
    }


    // M�thode pour activer ou d�sactiver le rendu de l'objet
    public void SetRenduObjet(bool active)
    {
        // R�cup�rer tous les rendus (Renderer) attach�s � l'objet
        Renderer[] rendus = GetComponentsInChildren<Renderer>(true);

        // Activer ou d�sactiver le rendu pour chaque composant Renderer
        foreach (Renderer rendu in rendus)
        {
            rendu.enabled = active;
        }
    }
    void Start()
    {
        this.flagSpawn = this.transform.position;

    }

    // Update is called once per frame
    void Update()
    {
    }

}



