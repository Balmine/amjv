using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceGunner : Unit
{
    // Start is called before the first frame update
    public AudioClip bulletSound;
    public GameObject bullet;
    public Material newDefaultMaterial;

    public Material newSelectedMaterial;


    void Start()
    {
        health = 5;
        movementSpeed = 5;
        base.Start();
        Renderer renderer = GetComponent<Renderer>();
        if (renderer != null)
        {
            renderer.material = newDefaultMaterial;
        }
        selectedMaterial = newSelectedMaterial;
    }

    // Update is called once per frame
    void Update()
    {
        if (state == "attacking" && targetUnit != null)
        {
            Vector3 pos = targetUnit.transform.position;
            float distanceToTarget = Vector3.Distance(transform.position, pos);

            if (distanceToTarget > attackRange)
            {
                pos.y = 0;

                updatePathTimer += Time.deltaTime;
                if (updatePathTimer >= updatePathInterval)
                {
                    this.Move(pos);
                    updatePathTimer = 0f;
                }
            }
            else
            {
                updatePathTimer = 1f;
                navMeshAgent.ResetPath();
                updateAttackTimer += Time.deltaTime;
                if (updateAttackTimer > attackCooldown)
                {
                    updateAttackTimer = 0f;
                    AudioSource.PlayClipAtPoint(bulletSound, transform.position);

                    float projectileSpeed = 10 * distanceToTarget / (2 * attackCooldown);
                    GameObject projectile = Instantiate(bullet, transform.position, Quaternion.identity);
                    BulletProjectile projectileScript = projectile.GetComponent<BulletProjectile>();
                    if (projectileScript != null)
                    {
                        projectileScript.targetUnit = targetUnit;
                        projectileScript.speed = projectileSpeed;
                        projectileScript.attackDamage = attackDamage;
                    }

                }
            }
        }
        else if (state == "attacking" && targetEnemy != null)
        {
            Vector3 pos = targetEnemy.transform.position;
            float distanceToTarget = Vector3.Distance(transform.position, pos);

            if (distanceToTarget > attackRange)
            {
                pos.y = 0;

                updatePathTimer += Time.deltaTime;
                if (updatePathTimer >= updatePathInterval)
                {
                    this.Move(pos);
                    updatePathTimer = 0f;
                }
            }
            else
            {
                updatePathTimer = 1f;
                navMeshAgent.ResetPath();
                updateAttackTimer += Time.deltaTime;
                if (updateAttackTimer > attackCooldown)
                {
                    updateAttackTimer = 0f;
                    AudioSource.PlayClipAtPoint(bulletSound, transform.position);

                    float projectileSpeed = 10 * distanceToTarget / (2 * attackCooldown);
                    GameObject projectile = Instantiate(bullet, transform.position, Quaternion.identity);
                    BulletProjectile projectileScript = projectile.GetComponent<BulletProjectile>();
                    if (projectileScript != null)
                    {
                        projectileScript.targetEnemy = targetEnemy;
                        projectileScript.speed = projectileSpeed;
                        projectileScript.attackDamage = attackDamage;
                    }

                }
            }
        }
    }
}
