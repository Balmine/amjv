using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnnemyAggroCheck : MonoBehaviour
{
    public GameObject PlayerTarget { get; set; }

    public Enemy _enemy;

    private void Awake()
    {
        PlayerTarget = GameObject.FindGameObjectWithTag("enemy");
        _enemy = GetComponentInParent<Enemy>();

    }
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "unit")
        {
            _enemy.player = collision.gameObject;
            _enemy.SetAggroStatus(true);
        }
    }
    private void OnTriggerExit(Collider collision)
    {
        if (collision.gameObject.tag == "unit")
        {
            _enemy.SetAggroStatus(false);
        }
    }

}
