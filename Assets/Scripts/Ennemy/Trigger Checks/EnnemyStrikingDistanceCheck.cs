using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnnemyStrikingDistanceCheck : MonoBehaviour
{
    public GameObject PlayerTarget { get; set; }
    private Enemy _enemy;

    private void Awake()
    {
        PlayerTarget = GameObject.FindGameObjectWithTag("enemy");
        _enemy = GetComponentInParent<Enemy>();

    }
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "unit")
        {
            _enemy.SetWithinStrikingDistance(true);
            _enemy.player = collision.gameObject;
        }
    }
    private void OnTriggerExit(Collider collision)
    {
        if (collision.gameObject.tag == "unit")
        {
            _enemy.SetWithinStrikingDistance(false);
        }
    }

}
