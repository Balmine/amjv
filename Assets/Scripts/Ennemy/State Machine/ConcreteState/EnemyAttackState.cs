using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UIElements;



public class EnemyAttackState : EnemyState
{
    private float _timer;
    private float _timeBetweenShots = 2f;
    private float _exitTimer;
    private float _timeTillExit = 3f;
    private float _distanceToCountExit = 3f;
    private Transform _playerTransform;
    public Enemy _enemy;
    public Unit unit;

    private float updatePathInterval = 5f;

    private float updateAttackTimer = 0f;
    public EnemyAttackState(Enemy enemy, EnemyStateMachine enemyStateMachine) : base(enemy, enemyStateMachine)
    {
    }

    public override void AnimationTriggerEvent()
    {
        base.AnimationTriggerEvent();

    }

    public override void AnimationTriggerEvent(Enemy.AnimationTriggerType triggerType)
    {
        base.AnimationTriggerEvent(triggerType);
    }

    public override void EnterState()
    {
        base.EnterState();
        _enemy = GameObject.Find("AggroSphere").GetComponent<EnnemyAggroCheck>()._enemy;
        _playerTransform = _enemy.player.transform;
        unit = _enemy.player.GetComponent<Unit>();

    }

    public override void ExitState()
    {
        base.ExitState();
    }

    public override void FrameUpdate()
    {
        if (unit != null)
        {


            base.FrameUpdate();
            Enemy.MoveEnemy(Vector3.zero);
            if (_timer > _timeBetweenShots)
            {
                _timer = 0f;

                unit.TakeDamage(Enemy.attackDamage);
            }
            if (Vector3.Distance(_playerTransform.position, Enemy.transform.position) > _distanceToCountExit)
            {
                _exitTimer += Time.deltaTime;
                if (_exitTimer > _timeTillExit)
                {
                    Enemy.StateMachine.ChangeState(Enemy.ChaseState);
                }
            }
            else
            {
                _exitTimer = 0f;
            }
            _timer += Time.deltaTime;
        }
    }



    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
