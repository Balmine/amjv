using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UIElements;

public class EnemyIdleState : EnemyState
{
    private Vector3 _targetPos;
    private NavMeshAgent agent;
    public EnemyIdleState(Enemy enemy, EnemyStateMachine enemyStateMachine) : base(enemy, enemyStateMachine)
    {
    }

    public override void AnimationTriggerEvent()
    {
        base.AnimationTriggerEvent();
    }

    public override void AnimationTriggerEvent(Enemy.AnimationTriggerType triggerType)
    {
        base.AnimationTriggerEvent(triggerType);
    }

    public override void EnterState()
    {
        base.EnterState();
        agent = Enemy.GetComponent<NavMeshAgent>();
        _targetPos = GetRandomPointOnNavMesh();
        agent.SetDestination(_targetPos);

    }

    public override void ExitState()
    {
        base.ExitState();
    }

    public override void FrameUpdate()
    {
        base.FrameUpdate();
        if (Enemy.IsAggroed)
        {
            agent.ResetPath();
            Enemy.StateMachine.ChangeState(Enemy.ChaseState);
            return;
        }
        Vector3 enemyPosFloored = new Vector3(Enemy.transform.position.x, 0f, Enemy.transform.position.z);

        if ((enemyPosFloored - _targetPos).sqrMagnitude < 0.5f)
        {
            _targetPos = GetRandomPointOnNavMesh();
            agent.SetDestination(_targetPos);

        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
    private Vector3 GetRandomPointOnNavMesh()
    {
        Vector3 randomDirection = Random.insideUnitSphere * 3f; // Adjust the radius as needed
        randomDirection += Enemy.transform.position;

        NavMeshHit hit;
        NavMesh.SamplePosition(randomDirection, out hit, 10f, NavMesh.AllAreas);
        return hit.position;
    }
}
