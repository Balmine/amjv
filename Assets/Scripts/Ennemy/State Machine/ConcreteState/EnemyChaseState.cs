using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using static UnityEngine.GraphicsBuffer;

public class EnemyChaseState : EnemyState
{
    private Transform _playerTransform;
    private NavMeshAgent agent;


    private float updateInterval = 0.5f;
    private float timeSinceLastUpdate = 0f;

    public Enemy _enemy;

    public EnemyChaseState(Enemy enemy, EnemyStateMachine enemyStateMachine) : base(enemy, enemyStateMachine)
    {

    }


    public override void AnimationTriggerEvent()
    {
        base.AnimationTriggerEvent();
    }

    public override void AnimationTriggerEvent(Enemy.AnimationTriggerType triggerType)
    {
        base.AnimationTriggerEvent(triggerType);
    }

    public override void EnterState()
    {
        _enemy = GameObject.Find("AggroSphere").GetComponent<EnnemyAggroCheck>()._enemy;
        base.EnterState();
        agent = Enemy.GetComponent<NavMeshAgent>();
    }

    public override void ExitState()
    {
        base.ExitState();
    }

    public override void FrameUpdate()
    {
        if (_enemy.player != null)
        {

            base.FrameUpdate();
            timeSinceLastUpdate += Time.deltaTime;
            if (timeSinceLastUpdate >= updateInterval)
            {
                _playerTransform = _enemy.player.transform;
                agent.SetDestination(_playerTransform.position);
                timeSinceLastUpdate = 0f;

            }

            if (Enemy.IsWithinStrikingDistance)
            {
                Enemy.StateMachine.ChangeState(Enemy.AttackState);
            }
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
