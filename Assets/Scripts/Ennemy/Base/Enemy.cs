using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour, IDamageable, IEnemyMoveable, ITriggerCheckable
{
    [field: SerializeField] public float MaxHealth { get; set; } = 100f;
    public float CurrentHealth { get; set; }
    public Rigidbody RB { get; set; }
    public EnemyStateMachine StateMachine { get; set; }
    public EnemyIdleState IdleState { get; set; }
    public EnemyAttackState AttackState { get; set; }
    public EnemyChaseState ChaseState { get; set; }
    public bool IsAggroed { get; set; }
    public bool IsWithinStrikingDistance { get; set; }

    public float RandomMovementRange = 5f;
    public float RandommovementSpeed = 1f;
    public int attackDamage = 5;
    public float health = 25f;

    public GameObject player;

    public HealthBar healthBar;

    public HealthBar healthBarPrefab;

    private HealthBar healthBarInstance;

    public GameObject redBar;

    public GameObject hbCanvas;



    public void Damage(float damageAmount)
    {
        CurrentHealth -= damageAmount;

        if (CurrentHealth <= 0f)
        {
            Die();
        }
    }
    public void TakeDamage(int damageAmount)
    {
        health -= damageAmount;

    }

    public void Heal(int healAmount)
    {
        health += healAmount;
    }

    public void Kill()
    {
        Destroy(gameObject);
    }

    public void Die()
    {
        Destroy(gameObject);
    }

    public void MoveEnemy(Vector3 velocity)
    {
        RB.MovePosition(transform.position + velocity * Time.deltaTime);
    }
    private void Awake()
    {
        StateMachine = new EnemyStateMachine();
        IdleState = new EnemyIdleState(this, StateMachine);
        AttackState = new EnemyAttackState(this, StateMachine);
        ChaseState = new EnemyChaseState(this, StateMachine);
    }
    private void Start()
    {
        hbCanvas = GameObject.Find("HBCanvas");

        CurrentHealth = MaxHealth;
        healthBarInstance = Instantiate(healthBarPrefab);
        redBar = Instantiate(redBar);
        redBar.transform.SetParent(hbCanvas.transform, false);
        healthBarInstance.enemy = this;
        healthBarInstance.redBar = redBar;

        healthBarInstance.transform.SetParent(hbCanvas.transform, false);
        redBar.transform.SetParent(hbCanvas.transform, false);

        // Nomme la barre de vie "HB {unit name}"
        healthBarInstance.name = "HB " + this.name;
        RB = GetComponent<Rigidbody>();

        StateMachine.Initialize(IdleState);



    }
    private void Update()
    {
        StateMachine.CurrentEnemyState.FrameUpdate();
    }
    private void FixedUpdate()
    {
        StateMachine.CurrentEnemyState.PhysicsUpdate();
    }

    private void AnimationTriggerEvent(AnimationTriggerType triggertype)
    {
        StateMachine.CurrentEnemyState.AnimationTriggerEvent(triggertype);
    }

    public void SetAggroStatus(bool isAggroed)
    {
        IsAggroed = isAggroed;
    }

    public void SetWithinStrikingDistance(bool isWithinStrikingDistance)
    {
        IsWithinStrikingDistance = isWithinStrikingDistance;
    }

    public enum AnimationTriggerType
    {
        EnemyDamaged,
        PlayFootstepSounds
    }
}
