using UnityEngine;
using UnityEngine.AI;

using System.Linq;

public class UnitController : MonoBehaviour
{
    public CustomList<Unit> unitsToMove;
    public float baseOffset = 0f;
    public float offsetMultiplier = 1.5f;
    public Vector3 offset = Vector3.zero;
    public float minDistance = 5f;
    public float unitsSpacing = 1.5f;

    public int layerMask;


    private void Start()
    {
        unitsToMove = new CustomList<Unit>();
        layerMask = ~(1 << LayerMask.NameToLayer("UI"));

    }
    void Update()
    {

        if (Input.GetMouseButtonUp(1))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitInfo;

            if (Physics.Raycast(ray, out hitInfo, float.MaxValue, layerMask))
            {

                if (hitInfo.collider != null)
                {
                    GameObject hitGameObject = hitInfo.collider.gameObject;
                    if (hitGameObject != null)
                    {
                        Unit hitUnit = hitGameObject.GetComponent<Unit>();
                        Enemy hitEnemy = hitGameObject.GetComponent<Enemy>();
                        if (hitUnit != null)
                        {

                            if (hitUnit.GetSide() != unitsToMove[0].GetSide())
                            {
                                foreach (Unit unit in unitsToMove)
                                {
                                    unit.targetUnit = hitUnit;
                                    unit.SetState("attacking");

                                }
                            }
                        }
                        else if (hitEnemy != null)
                        {

                            foreach (Unit unit in unitsToMove)
                            {
                                unit.targetEnemy = hitEnemy;
                                unit.SetState("attacking");

                            }

                        }
                        else
                        {
                            foreach (Unit unit in unitsToMove)
                            {
                                unit.targetUnit = null;
                                unit.targetEnemy = null;
                                unit.SetState("moving");
                            }
                            PlaceUnitsInSpiralGrid(unitsToMove, hitInfo.point, unitsSpacing);
                        }
                    }
                    else
                    {
                        PlaceUnitsInSpiralGrid(unitsToMove, hitInfo.point, unitsSpacing);
                    }

                }
            }

        }

    }
    public void DeselectAllUnits()
    {
        CustomList<Unit> unitsCopy = new CustomList<Unit>();
        unitsCopy.AddRange(unitsToMove);
        foreach (Unit unit in unitsCopy)
        {
            unit.SetSelected(false);
        }

        unitsToMove.Clear();
    }

    Vector3 CalculateCenterOfSwarm(CustomList<Unit> units)
    {
        Vector3 center = Vector3.zero;

        foreach (Unit unit in units)
        {
            center += unit.transform.position;
        }

        return center / units.Count;
    }


    public void PlaceUnitsInSpiralGrid(CustomList<Unit> units, Vector3 centralPoint, float spacing)
    {
        int gridX = 0;
        int gridZ = 0;
        int xDirection = 1;
        int zDirection = 0;
        int stepsUntilDirectionChange = 1;
        int stepsTaken = 0;

        foreach (Unit unit in units)
        {
            bool positionFound = false;
            int maxSteps = 0;
            while (!positionFound && maxSteps < 100)
            {
                maxSteps++;
                Vector3 unitPosition = centralPoint + new Vector3(gridX * spacing, 0f, gridZ * spacing);

                if (unit.Move(unitPosition))
                {
                    unit.SetState("moving");
                    positionFound = true;
                }

                gridX += xDirection;
                gridZ += zDirection;
                stepsTaken++;

                if (stepsTaken == stepsUntilDirectionChange)
                {
                    stepsTaken = 0;
                    int temp = xDirection;
                    xDirection = -zDirection;
                    zDirection = temp;

                    if (zDirection == 0)
                    {
                        stepsUntilDirectionChange++;
                    }
                }

            }
        }
    }


}