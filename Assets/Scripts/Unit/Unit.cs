using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Unit : MonoBehaviour
{
    [SerializeField] public int health;
    public int maxHealth = 100;
    public float movementSpeed;
    public NavMeshAgent navMeshAgent;
    public bool selected;

    public int SpecialAttackDamage = 20;
    private float lastSpecialAttack;
    private float detectionRadius = 15f;
    private bool isRPressed = false;


    public bool hasFlag;
    public UnitController unitController;
    public Material selectedMaterial;
    private Material defaultMaterial;
    public float attackRange;
    public int attackDamage;
    public string state = "idle"; //State can have few values : idle, moving, attacking, ...
    [SerializeField] public string side; // le camp de l'unit�, peut valloir red blue green ...
    public Flag flag;
    public float attackCooldown = 2.0f;
    public Unit targetUnit;
    public float updatePathInterval = 0.5f;
    public float updatePathTimer = 0f;
    public float updateAttackTimer = 0f;
    [SerializeField] public GameObject spear;
    public RTSManager RTSManager;
    public HealthBar healthBar;

    public HealthBar healthBarPrefab;

    private HealthBar healthBarInstance;

    public GameObject redBar;

    public Enemy targetEnemy;

    public GameObject hbCanvas;

    public Material flagMaterial;




    protected virtual void Start()
    {
        hbCanvas = GameObject.Find("HBCanvas");

        UnitManager.instance.RegisterUnit(this);
        navMeshAgent = GetComponent<NavMeshAgent>();
        if (navMeshAgent == null)
        {
            navMeshAgent = gameObject.AddComponent<NavMeshAgent>();
        }

        navMeshAgent.speed = movementSpeed;
        defaultMaterial = GetComponent<Renderer>().material;
        updatePathTimer = updatePathInterval;
        updateAttackTimer = attackRange;
        health = maxHealth;

        healthBarInstance = Instantiate(healthBarPrefab);
        redBar = Instantiate(redBar);
        redBar.transform.SetParent(hbCanvas.transform, false);
        healthBarInstance.unit = this;
        healthBarInstance.redBar = redBar;

        healthBarInstance.transform.SetParent(hbCanvas.transform, false);
        redBar.transform.SetParent(hbCanvas.transform, false);

        // Nomme la barre de vie "HB {unit name}"
        healthBarInstance.name = "HB " + this.name;
    }
    public void TakeDamage(int damageAmount)
    {
        health -= damageAmount;
        if (health <= 0)
        {
            this.Kill();
        }

    }

    public void Heal(int healAmount)
    {
        health += healAmount;
    }

    public void SetSelected(bool select)
    {
        selected = select;

        if (select)
        {
            GetComponent<Renderer>().material = selectedMaterial;
            if (!unitController.unitsToMove.Contains(this))
            {
                unitController.unitsToMove.Add(this);
            }
        }
        else if (!select)
        {
            GetComponent<Renderer>().material = defaultMaterial;
            if (unitController.unitsToMove.Contains(this))
            {
                unitController.unitsToMove.Remove(this);
            }
        }
    }

    public bool Move(Vector3 destination)
    {
        if (NavMesh.SamplePosition(destination, out NavMeshHit hit, 1.0f, NavMesh.AllAreas))
        {
            navMeshAgent.SetDestination(hit.position);
            return true;
        }
        else
        {
            return false;
        }
    }

    public void Kill()
    {
        if (UnitManager.instance != null)
        {
            if (this.hasFlag)
            {
                flag = GameObject.Find("Flag").GetComponent<Flag>();
                flag.isCaptured = false;
                flag.transform.parent = null;
                flag.SetRenduObjet(true);
                RTSManager.TriggerDefeat();
            }

            UnitManager.instance.UnregisterUnit(this);
            if (unitController.unitsToMove.Contains(this))
            {
                unitController.unitsToMove.Remove(this);
            }
        }
        Destroy(gameObject);
    }

    public void Attack(int DamageDealt, Unit target)
    {
        if (target != null)
        {
            target.TakeDamage(DamageDealt);

            if (target == null)
            {
                state = "idle";
            }

        }
        else
        {
            state = "idle";
        }
    }

    public string GetSide()
    {
        return side;
    }
    public void SetSide(string newSide)
    {
        side = newSide;
    }

    public void SetState(string newState)
    {
        state = newState;
    }

    public string GetState()
    {
        return state;
    }

    void FindEnemiesInRadius()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, detectionRadius);

        foreach (Collider collider in colliders)
        {
            Enemy enemy = collider.GetComponent<Enemy>();

            if (enemy != null)
            {
                enemy.TakeDamage(SpecialAttackDamage);

            }
        }
    }

    void OnDrawGizmosSelected()
    {
        // Dessine une sphère dans l'éditeur Unity pour représenter le rayon de détection
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, detectionRadius);
    }

    private void Update()
    {

        if (state == "attacking" && targetUnit != null)
        {
            Vector3 pos = targetUnit.transform.position;
            float distanceToTarget = Vector3.Distance(transform.position, pos);

            if (distanceToTarget > attackRange)
            {
                pos.y = 0;

                updatePathTimer += Time.deltaTime;
                if (updatePathTimer >= updatePathInterval)
                {
                    this.Move(pos);
                    updatePathTimer = 0f;
                }
            }
            else
            {
                updatePathTimer = 1f;
                navMeshAgent.ResetPath();
                updateAttackTimer += Time.deltaTime;
                if (updateAttackTimer > attackCooldown)
                {
                    updateAttackTimer = 0f;

                    float projectileSpeed = distanceToTarget / (2 * attackCooldown);
                    float gravity = 9.8f / attackCooldown;
                    GameObject projectile = Instantiate(spear, transform.position, Quaternion.identity);
                    SpearProjectile projectileScript = projectile.GetComponent<SpearProjectile>();
                    if (projectileScript != null)
                    {
                        projectileScript.targetUnit = targetUnit;
                        projectileScript.speed = projectileSpeed;
                        projectileScript.attackDamage = attackDamage;
                    }



                }
            }
        }
        else if (state == "attacking" && targetEnemy != null)
        {
            Vector3 pos = targetEnemy.transform.position;
            float distanceToTarget = Vector3.Distance(transform.position, pos);

            if (distanceToTarget > attackRange)
            {
                pos.y = 0;

                updatePathTimer += Time.deltaTime;
                if (updatePathTimer >= updatePathInterval)
                {
                    this.Move(pos);
                    updatePathTimer = 0f;
                }
            }
            else
            {
                updatePathTimer = 1f;
                navMeshAgent.ResetPath();
                updateAttackTimer += Time.deltaTime;
                if (updateAttackTimer > attackCooldown)
                {
                    updateAttackTimer = 0f;

                    float projectileSpeed = distanceToTarget / (2 * attackCooldown);
                    float gravity = 9.8f / attackCooldown;
                    GameObject projectile = Instantiate(spear, transform.position, Quaternion.identity);
                    SpearProjectile projectileScript = projectile.GetComponent<SpearProjectile>();
                    if (projectileScript != null)
                    {
                        projectileScript.targetEnemy = targetEnemy;
                        projectileScript.speed = projectileSpeed;
                        projectileScript.attackDamage = attackDamage;
                    }



                }
            }
        }
        else if (state == "attacking" && targetEnemy != null)
        {
            Vector3 pos = targetEnemy.transform.position;
            float distanceToTarget = Vector3.Distance(transform.position, pos);

            if (distanceToTarget > attackRange)
            {
                pos.y = 0;

                updatePathTimer += Time.deltaTime;
                if (updatePathTimer >= updatePathInterval)
                {
                    this.Move(pos);
                    updatePathTimer = 0f;
                }
            }
            else
            {
                updatePathTimer = 1f;
                navMeshAgent.ResetPath();
                updateAttackTimer += Time.deltaTime;
                if (updateAttackTimer > attackCooldown)
                {
                    updateAttackTimer = 0f;

                    float projectileSpeed = distanceToTarget / (2 * attackCooldown);
                    float gravity = 9.8f / attackCooldown;
                    GameObject projectile = Instantiate(spear, transform.position, Quaternion.identity);
                    SpearProjectile projectileScript = projectile.GetComponent<SpearProjectile>();
                    if (projectileScript != null)
                    {
                        projectileScript.targetEnemy = targetEnemy;
                        projectileScript.speed = projectileSpeed;
                        projectileScript.attackDamage = attackDamage;
                    }



                }
            }
        }
        if (hasFlag)
        {
            // Si la touche "R" est maintenue
            if (Input.GetKey(KeyCode.R))
            {
                // Appelle OnDrawGizmosSelected()
                isRPressed = true;
            }
            // Si la touche "R" est relâchée
            else if (Input.GetKeyUp(KeyCode.R))
            {
                isRPressed = false;
                if (Time.time - lastSpecialAttack >= 20f)
                {
                    FindEnemiesInRadius();
                    lastSpecialAttack = Time.time;
                }
            }
        }
    }

    public void ChangeMaterialWhenFlag()
    {
        if (hasFlag)
        {
            GetComponent<Renderer>().material = flagMaterial;
        }
        else
        {
            GetComponent<Renderer>().material = defaultMaterial;
        }
    }
}
