using UnityEngine;
using UnityEngine.UI;


public class UnitSelection : MonoBehaviour
{
    public float selectionRadius = 1.5f;
    public UnitController unitController;
    private bool isDragging = false;
    private Vector3 initialMousePosition;
    private Vector3 initialMousePositionOnScreen;
    private Vector3 finalMousePositionOnScreen;
    private Vector3 finalMousePosition;
    public Camera mainCamera;
    public int layerMask;

    void Start()
    {
        layerMask = ~(1 << LayerMask.NameToLayer("UI"));
    }
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, float.MaxValue, layerMask))
            {
                Unit selectedUnit = hit.collider.GetComponent<Unit>();
                if (selectedUnit != null && selectedUnit.side == "blue")
                {
                    if (Input.GetKey(KeyCode.LeftControl))
                    {
                        if (unitController.unitsToMove.Contains(selectedUnit))
                        {
                            unitController.unitsToMove.Remove(selectedUnit);
                            DeselectUnit(selectedUnit);

                        }
                        else
                        {
                            unitController.unitsToMove.Add(selectedUnit);
                            SelectUnit(selectedUnit);
                        }
                    }
                    else
                    {
                        if (!Input.GetKey(KeyCode.LeftControl))
                        {
                            unitController.DeselectAllUnits();
                        }
                        unitController.unitsToMove.Add(selectedUnit);
                        SelectUnit(selectedUnit);
                    }
                }
                else
                {
                    if (!Input.GetKey(KeyCode.LeftControl))
                    {
                        unitController.DeselectAllUnits();
                    }
                }
            }
        }
        if (Input.GetMouseButtonDown(0) && !isDragging)
        {
            isDragging = true;

            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, float.MaxValue, layerMask))
            {
                initialMousePositionOnScreen = Input.mousePosition;
                initialMousePosition = new Vector3(hit.point.x, 0f, hit.point.z);

            }
        }

        else if (Input.GetMouseButtonUp(0) && isDragging)
        {
            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, float.MaxValue, layerMask))
            {
                finalMousePositionOnScreen = Input.mousePosition;
                finalMousePosition = new Vector3(hit.point.x, 0f, hit.point.z);
                SelectUnitsInArea(initialMousePosition, finalMousePosition, initialMousePositionOnScreen, finalMousePositionOnScreen);
            }
            isDragging = false;
        }

    }


    void SelectUnit(Unit unitToSelect)
    {
        unitToSelect.SetSelected(true);
    }
    void DeselectUnit(Unit unitToSelect)
    {
        unitToSelect.SetSelected(false);
    }
    //public void ShowSelectionVisualEffect(Unit unit)
    //{
    //    GameObject effect = Instantiate(selectionVisualEffectPrefab, unit.transform.position, Quaternion.identity);
    //    effect.transform.SetParent(unit.transform);
    //    Destroy(effect, 2.0f);
    //}

    //public void ShowClickVisualEffect(Vector3 position)
    //{
    //    Instantiate(clickEffectPrefab, position, Quaternion.identity);
    //}


    public void SelectUnitsInArea(Vector3 initialPosition, Vector3 finalPosition, Vector3 initialPositionOnScreen, Vector3 finalPositionOnScreen)
    {

        Vector3 topLeftProjected = new Vector3(Mathf.Min(initialPosition.x, finalPosition.x), 0f, Mathf.Max(initialPosition.z, finalPosition.z));
        Vector3 bottomRightProjected = new Vector3(Mathf.Max(initialPosition.x, finalPosition.x), 0f, Mathf.Min(initialPosition.z, finalPosition.z));
        Vector3 bottomLeft = new Vector3(Mathf.Min(initialPositionOnScreen.x, finalPositionOnScreen.x), Mathf.Min(initialPositionOnScreen.y, finalPositionOnScreen.y), 0f);
        Vector3 topRight = new Vector3(Mathf.Max(initialPositionOnScreen.x, finalPositionOnScreen.x), Mathf.Max(initialPositionOnScreen.y, finalPositionOnScreen.y, 0f));
        Ray ray1 = mainCamera.ScreenPointToRay(bottomLeft);
        RaycastHit hit1;
        Vector3 bottomLeftProjected = new Vector3();
        if (Physics.Raycast(ray1, out hit1))
        {
            bottomLeftProjected = new Vector3(hit1.point.x, 0f, hit1.point.z);
        }
        Ray ray2 = mainCamera.ScreenPointToRay(topRight);
        RaycastHit hit2;
        Vector3 topRightProjected = new Vector3();
        if (Physics.Raycast(ray2, out hit2))
        {
            topRightProjected = new Vector3(hit2.point.x, 0f, hit2.point.z);
        }
        float gBC(float x)
        {
            return (bottomRightProjected.z - topRightProjected.z) / (bottomRightProjected.x - topRightProjected.x) * x + topRightProjected.z;
        }

        float gAB(float x)
        {
            return (topLeftProjected.z - topRightProjected.z) / (topLeftProjected.x - topRightProjected.x) * x + topLeftProjected.z;
        }

        float gCD(float x)
        {
            return (bottomLeftProjected.z - bottomRightProjected.z) / (bottomLeftProjected.x - bottomRightProjected.x) * x + bottomRightProjected.z;
        }

        float gAD(float x)
        {
            return (topLeftProjected.z - bottomLeftProjected.z) / (topLeftProjected.x - bottomLeftProjected.x) * x + bottomLeftProjected.z;
        }

        float gprimeBC(float x)
        {
            return (x - topRightProjected.z) / ((bottomRightProjected.z - topRightProjected.z) / (bottomRightProjected.x - topRightProjected.x));
        }

        float gprimeAB(float x)
        {
            return (x - topLeftProjected.z) / ((topLeftProjected.z - topRightProjected.z) / (topLeftProjected.x - topRightProjected.x));
        }

        float gprimeCD(float x)
        {
            return (x - bottomRightProjected.z) / ((bottomLeftProjected.z - bottomRightProjected.z) / (bottomLeftProjected.x - bottomRightProjected.x));
        }

        float gprimeAD(float x)
        {
            return (x - bottomLeftProjected.z) / ((topLeftProjected.z - bottomLeftProjected.z) / (topLeftProjected.x - bottomLeftProjected.x));
        }

        foreach (Unit unit in UnitManager.instance.allUnits)
        {
            Vector3 unitPosition = unit.transform.position;

            if (unitPosition.x <= gprimeBC(unitPosition.z) + topRightProjected.x && unitPosition.x >= gprimeAB(unitPosition.z) + topLeftProjected.x &&
                unitPosition.x <= gprimeCD(unitPosition.z) + bottomRightProjected.x && unitPosition.x >= gprimeAD(unitPosition.z) + bottomLeftProjected.x &&
                unitPosition.z <= gAB(unitPosition.x - topLeftProjected.x) && unitPosition.z >= gAD(unitPosition.x - bottomLeftProjected.x) &&
                unitPosition.z <= gBC(unitPosition.x - topRightProjected.x) && unitPosition.z >= gCD(unitPosition.x - bottomRightProjected.x))

            {
                if (!unitController.unitsToMove.Contains(unit) && unit.side == "blue")
                {
                    unitController.unitsToMove.Add(unit);
                    unit.SetSelected(true);
                }
            }
        }

    }


    private void OnGUI()
    {
        if (isDragging == true)
        {
            DrawSelectionBox(initialMousePositionOnScreen, Input.mousePosition, new Color(0.8f, 0.8f, 0.95f, 0.25f), new Color(0.8f, 0.8f, 0.95f));
        }
    }

    void DrawSelectionBox(Vector2 start, Vector2 end, Color boxColor, Color borderColor)
    {
        float width = Mathf.Abs(end.x - start.x);
        float height = Mathf.Abs(end.y - start.y);

        float left = Mathf.Min(start.x, end.x);
        float top = Screen.height - Mathf.Max(start.y, end.y);
        Rect rect = new Rect(left, top, width, height);

        GUIStyle style = new GUIStyle();
        Texture2D texture = new Texture2D(1, 1);
        texture.SetPixel(0, 0, boxColor);
        texture.Apply();
        style.normal.background = texture;
        GUI.Box(rect, GUIContent.none, style);

        GL.PushMatrix();
        GL.Begin(GL.LINES);
        GL.Color(borderColor);
        GL.Vertex3(left, top, 0);
        GL.Vertex3(left + width, top, 0);
        GL.Vertex3(left + width, top, 0);
        GL.Vertex3(left + width, top + height, 0);
        GL.Vertex3(left + width, top + height, 0);
        GL.Vertex3(left, top + height, 0);
        GL.Vertex3(left, top + height, 0);
        GL.Vertex3(left, top, 0);
        GL.End();
        GL.PopMatrix();
    }



}
