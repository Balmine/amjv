using UnityEngine;

public class UnitAnimator : MonoBehaviour
{
    private Animator animator;

    private void Start()
    {
        // Get the Animator component attached to this GameObject
        animator = GetComponent<Animator>();
        PlayAnimation("Idle");
        Debug.Log("Animator: " + animator);
    }

    public void PlayAnimation(string animationName)
    {
        // Play the animation with the given name
        if (animator != null)
        {
            animator.Play(animationName);
        }
    }
}