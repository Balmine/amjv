using UnityEngine;

public class SpearProjectile : MonoBehaviour
{
    public float speed = 5.0f;
    public float arcHeight = 2.0f;
    public Unit targetUnit;
    private Vector3 initialPosition;
    private Vector3 previousPosition;
    private float startTime;
    public int attackDamage;
    public AudioClip launchSound; // assign in inspector
    public AudioClip hitSound; // assign in inspector

    public Enemy targetEnemy;
    void Start()
    {
        startTime = Time.time;
        initialPosition = transform.position;
        previousPosition = initialPosition;
        AudioSource.PlayClipAtPoint(launchSound, transform.position);
    }

    void Update()
    {
        if (targetUnit == null && targetEnemy == null)
        {
            Destroy(gameObject);
            return;
        }
        if (targetUnit != null)

        {
            float elapsedTime = Time.time - startTime;


            Vector3 targetPosition = targetUnit.transform.position;

            float t = elapsedTime * speed;
            float x = Mathf.Lerp(initialPosition.x, targetPosition.x, t);
            float y = initialPosition.y + Mathf.Sin(Mathf.Lerp(0, Mathf.PI, t)) * arcHeight;
            float z = Mathf.Lerp(initialPosition.z, targetPosition.z, t);

            transform.position = new Vector3(x, y, z);

            Vector3 direction = transform.position - previousPosition;

            if (direction != Vector3.zero)
            {
                transform.rotation = Quaternion.LookRotation(Vector3.up, direction.normalized);
            }

            previousPosition = transform.position;

            if (t >= 1.0f)
            {
                AudioSource.PlayClipAtPoint(hitSound, transform.position);

                if (targetUnit.health > attackDamage)
                {
                    targetUnit.TakeDamage(attackDamage);
                }
                else
                {
                    targetUnit.Kill();
                }

                Destroy(gameObject);
            }
        }
        else
        {

            float elapsedTime = Time.time - startTime;


            Vector3 targetPosition = targetEnemy.transform.position;

            float t = elapsedTime * speed;
            float x = Mathf.Lerp(initialPosition.x, targetPosition.x, t);
            float y = initialPosition.y + Mathf.Sin(Mathf.Lerp(0, Mathf.PI, t)) * arcHeight;
            float z = Mathf.Lerp(initialPosition.z, targetPosition.z, t);

            transform.position = new Vector3(x, y, z);

            Vector3 direction = transform.position - previousPosition;

            if (direction != Vector3.zero)
            {
                transform.rotation = Quaternion.LookRotation(Vector3.up, direction.normalized);
            }

            previousPosition = transform.position;

            if (t >= 1.0f)
            {
                AudioSource.PlayClipAtPoint(hitSound, transform.position);

                if (targetEnemy.CurrentHealth > attackDamage)
                {
                    targetEnemy.Damage(attackDamage);
                }
                else
                {
                    targetEnemy.Die();
                }

                Destroy(gameObject);
            }
        }
    }


}
