using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RTSManager : MonoBehaviour
{
    public bool gameWon = false;
    public bool gameLost = false;
    public Text resultText;

    void Update()
    {
        Unit[] instances = Object.FindObjectsOfType<Unit>();

        if (instances.Length <= 0)
        {
           this.TriggerDefeat();
        }


        if (gameWon)
        {
            // Afficher le message de victoire
            SceneManager.LoadScene("VictoryScene");
        }
        else if (gameLost)
        {
            // Afficher le message de d�faite
            SceneManager.LoadScene("DefeatScene");
        }
    }

    // Exemple de m�thode pour d�clencher la victoire
    public void TriggerVictory()
    {
        gameWon = true;
        Debug.Log("C'est win");
    }

    // Exemple de m�thode pour d�clencher la d�faite
    public void TriggerDefeat()
    {
        gameLost = true;
        Debug.Log("C'est loose");
    }
}