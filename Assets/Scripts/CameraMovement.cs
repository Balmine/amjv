using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public float moveSpeed = 20f;
    public float zoomSpeed = 5.0f;
    public float minY = 0.0f;
    public float maxY = 200.0f;

    void Update()
    {
        // Get input for movement
        float forwardInput = Input.GetAxis("Vertical");
        float sidewaysInput = Input.GetAxis("Horizontal");

        // Calculate movement direction in local space
        Vector3 moveDirectionLocal = new Vector3(sidewaysInput, 0f, forwardInput);

        // Create a rotation that has the same Y rotation as the camera
        Quaternion yRotation = Quaternion.Euler(0f, transform.eulerAngles.y, 0f);

        // Transform the movement direction to world space
        Vector3 moveDirectionWorld = yRotation * moveDirectionLocal.normalized;

        // Move the camera
        transform.Translate(moveDirectionWorld * moveSpeed * Time.deltaTime, Space.World);

        float scrollDelta = Input.GetAxis("Mouse ScrollWheel");
        if (scrollDelta != 0.0f)
        {
            // Calculate new camera position
            Vector3 zoomDirection = transform.forward * scrollDelta * zoomSpeed;
            Vector3 newPosition = transform.position + zoomDirection;

            // Adjust camera position using the new position
            transform.position = newPosition;
        }
    }
}