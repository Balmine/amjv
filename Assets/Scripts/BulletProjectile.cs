using UnityEngine;

public class BulletProjectile : MonoBehaviour
{
    public Unit targetUnit;
    public float speed;
    public int attackDamage;

    public Enemy targetEnemy;

    private void Update()
    {
        if (targetUnit == null && targetEnemy == null)
        {
            // Destroy the bullet if there is no target
            Destroy(gameObject);
            return;
        }
        if (targetUnit != null)
        {

            // Move the bullet towards the target
            transform.position = Vector3.MoveTowards(transform.position, targetUnit.transform.position, speed * Time.deltaTime);

            // Check if the bullet has reached the target
            if (transform.position == targetUnit.transform.position)
            {
                // Apply damage to the target
                if (targetUnit.health > attackDamage)
                {
                    targetUnit.TakeDamage(attackDamage);
                }
                else
                {
                    targetUnit.Kill();
                }

                // Destroy the bullet
                Destroy(gameObject);

            }
        }
        if (targetEnemy != null)
        {

            // Move the bullet towards the target
            transform.position = Vector3.MoveTowards(transform.position, targetEnemy.transform.position, speed * Time.deltaTime);

            // Check if the bullet has reached the target
            if (transform.position == targetEnemy.transform.position)
            {
                // Apply damage to the target
                if (targetEnemy.CurrentHealth > attackDamage)
                {
                    targetEnemy.Damage(attackDamage);
                }
                else
                {
                    targetEnemy.Die();
                }

                // Destroy the bullet
                Destroy(gameObject);

            }
        }
    }
}