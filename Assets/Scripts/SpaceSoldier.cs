using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class SpaceSoldier : Unit
{

    public AudioClip hitSound;
    public Material newMaterial;
    public Material newSelectedMaterial;

    void Start()

    {
        health = 10;
        movementSpeed = 5;
        base.Start();
        Renderer renderer = GetComponent<Renderer>();
        if (renderer != null)
        {
            renderer.material = newMaterial;
        }
        selectedMaterial = newSelectedMaterial;

    }

    void Update()
    {
        if (state == "attacking" && targetUnit != null)
        {
            Vector3 pos = targetUnit.transform.position;
            float distanceToTarget = Vector3.Distance(transform.position, pos);

            if (distanceToTarget > attackRange)
            {
                pos.y = 0;

                updatePathTimer += Time.deltaTime;
                if (updatePathTimer >= updatePathInterval)
                {
                    this.Move(pos);
                    updatePathTimer = 0f;
                }
            }
            else
            {
                updatePathTimer = 1f;
                navMeshAgent.ResetPath();
                updateAttackTimer += Time.deltaTime;
                if (updateAttackTimer > attackCooldown)
                {
                    updateAttackTimer = 0f;
                    AudioSource.PlayClipAtPoint(hitSound, transform.position);
                    if (targetUnit.health > attackDamage)
                    {
                        targetUnit.TakeDamage(attackDamage);
                    }
                    else
                    {
                        targetUnit.Kill();
                    }

                }
            }
        }
        if (state == "attacking" && targetEnemy != null)
        {
            Vector3 pos = targetEnemy.transform.position;
            float distanceToTarget = Vector3.Distance(transform.position, pos);

            if (distanceToTarget > attackRange)
            {
                pos.y = 0;

                updatePathTimer += Time.deltaTime;
                if (updatePathTimer >= updatePathInterval)
                {
                    this.Move(pos);
                    updatePathTimer = 0f;
                }
            }
            else
            {
                updatePathTimer = 1f;
                navMeshAgent.ResetPath();
                updateAttackTimer += Time.deltaTime;
                if (updateAttackTimer > attackCooldown)
                {
                    updateAttackTimer = 0f;
                    AudioSource.PlayClipAtPoint(hitSound, transform.position);
                    if (targetEnemy.CurrentHealth > attackDamage)
                    {
                        targetEnemy.Damage(attackDamage);
                    }
                    else
                    {
                        targetEnemy.Die();
                    }

                }
            }
        }
    }


}
